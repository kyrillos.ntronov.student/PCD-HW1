package pcd.ass1.gui;

import java.io.File;

public class WordCounterParameters {

	private File directory;

	private File exclusionFile;

	private int virtualCoresNumber;

	private int mostFrequentWordsNumber;

	public File getDirectory() {
		return directory;
	}

	public void setDirectory(File file) {
		this.directory = file;
	}

	public File getExclusionFile() {
		return exclusionFile;
	}

	public int getVirtualCoresNumber() {
		return virtualCoresNumber;
	}

	public void setVirtualCoresNumber(int virtualCoresNumber) {
		this.virtualCoresNumber = virtualCoresNumber;
	}

	public int getMostFrequentWordsNumber() {
		return mostFrequentWordsNumber;
	}

	public void setMostFrequentWordsNumber(int mostFrequentWordsNumber) {
		this.mostFrequentWordsNumber = mostFrequentWordsNumber;
	}

	public void setExclusionFile(File exclusionFile) {
		this.exclusionFile = exclusionFile;
	}

}
