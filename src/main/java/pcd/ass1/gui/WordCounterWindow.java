package pcd.ass1.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import pcd.ass1.logging.Log;
import pcd.ass1.wordcounter.driver.WordCounterDriver;

public class WordCounterWindow {

	private static final Integer WINDOW_WIDTH = 2400;
	private static final Integer WINDOW_HEIGHT = 600;

	private final JFrame frame;
	private final JPanel panel;
	private final JScrollPane scrollPane;

	private final JFileChooser filesDirectory;
	private final JTextField outputWordsNumber;
	private final JFileChooser exclusionFile;

	private WordCounterDriver wordCounter;

	private final JButton startButton;
	private final JButton pauseButton;
	private final JButton resumeButton;

	private final JTextField wordsCounted;
	private final JTextArea outputArea;

	public WordCounterWindow() {
		this.frame = new JFrame("PCD Assignment 1");
		setUpFrame();
		this.panel = new JPanel();
		setUpPanel();
		this.scrollPane = new JScrollPane(panel);
		setUpScrollPane();
		this.filesDirectory = addChooserWithLabel("PDF files directory: ");
		setUpFilesDirectory();
		this.exclusionFile = addChooserWithLabel("Optional path to the words exclusion file");
		this.outputWordsNumber = addFieldWithLabel("Number of the most frequent words in output: ");
		this.startButton = addButton("Start");
		this.pauseButton = addButton("Pause");
		this.resumeButton = addButton("Resume");

		this.wordsCounted = addFieldWithLabel("Words Counted");
		this.wordsCounted.setEditable(false);
		this.outputArea = addTextAreaWithLabel("Words Occurencies");
		this.outputArea.setEditable(false);

		setUpButtons();
		
		this.frame.pack();
		this.frame.setVisible(true);
	}

	private void setUpFrame() {
		frame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void setUpPanel() {
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
	}

	private void setUpScrollPane() {
		scrollPane.setPreferredSize(new Dimension(500, 500));
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
	}

	private void setUpFilesDirectory() {
		filesDirectory.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		filesDirectory.setAcceptAllFileFilterUsed(false);
	}

	private void setUpButtons() {
		startButton.addActionListener(event -> {

			try {
				resetTextFiledsBorderColors();
				verifyFieldsAreValid();
				clearLogAndOutput();
				startButton.setEnabled(false);
				compute();
			} catch (Exception e) {
				handleCountErrorException(e);
			}

		});

		pauseButton.addActionListener(event -> {
			try {
				stop();
			} catch (Exception e) {
				handleCountErrorException(e);
			}
		});

		resumeButton.addActionListener(event -> {
			try {
				resume();
			} catch (Exception e) {
				handleCountErrorException(e);
			}
		});
	}

	private void compute() {
		Integer mostFrequentWordsNumber = Integer.parseInt(outputWordsNumber.getText().trim());
		WordCounterParameters parameters = new WordCounterParameters();
		parameters.setDirectory(filesDirectory.getSelectedFile());
		parameters.setExclusionFile(exclusionFile.getSelectedFile());
		parameters.setMostFrequentWordsNumber(mostFrequentWordsNumber);
		parameters.setVirtualCoresNumber(Runtime.getRuntime().availableProcessors() - Thread.activeCount());
		this.wordCounter = new WordCounterDriver(parameters, outputArea, wordsCounted, startButton);

		wordCounter.execute();
	}

	private void stop() {
		wordCounter.pauseCount();
	}

	private void resume() {
		wordCounter.resumeCount();
	}

	private JTextArea addTextAreaWithLabel(String labelText) {
		JTextArea textArea = new JTextArea(5, 20);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setBorder(new LineBorder(Color.GRAY, 1));
		textArea.setBackground(Color.BLACK);
		textArea.setForeground(Color.WHITE);
		addComponentWithLabel(textArea, labelText);
		return textArea;
	}

	private JTextField addFieldWithLabel(String labelText) {
		JTextField field = new JTextField();
		addComponentWithLabel(field, labelText);
		return field;
	}

	private JFileChooser addChooserWithLabel(String labelText) {
		JFileChooser chooser = new JFileChooser();
		addComponentWithLabel(chooser, labelText);
		return chooser;
	}

	private void addComponentWithLabel(JComponent component, String labelText) {
		JLabel label = new JLabel(labelText);
		label.setLabelFor(component);

		panel.add(label);
		panel.add(component);
	}

	private JButton addButton(String buttonText) {
		JButton button = new JButton(buttonText);
		panel.add(button);
		return button;
	}

	private void handleCountErrorException(Exception e) {
		String message = String.format("An exception has occured when counting most frequent words: %s", e);
		Log.error(message, "MAIN");
		outputErrorInTable(e, message);
	}

	private void verifyFieldsAreValid() {
		if (filesDirectory.getSelectedFile() == null) {
			handleInvalidInput(filesDirectory, "Files directory must be specified!");
		}
		if (!filesDirectory.getSelectedFile().isDirectory()) {
			handleInvalidInput(filesDirectory, "Files directory must be a directory!");
		}
		if (outputWordsNumber.getText().trim().isEmpty()) {
			handleInvalidInput(outputWordsNumber, "Output number of words must be specified!");
		}
		if (!isInteger(outputWordsNumber.getText().trim())) {
			handleInvalidInput(outputWordsNumber, "Output number of words must be a number!");
		}
		if (exclusionFile.getSelectedFile() != null && !exclusionFile.getSelectedFile().isFile()) {
			handleInvalidInput(exclusionFile, "Provided exclusion file must be a file!");
		}
	}

	private void handleInvalidInput(JComponent component, String message) {
		component.setBorder(new LineBorder(Color.RED, 2));
		throw new IllegalArgumentException(message);
	}

	private void clearLogAndOutput() {
		outputArea.setText("");
	}

	private void outputErrorInTable(Exception e, String message) {
		outputArea.setText(String.format("Exception: %s, Message: %s", e, message));
	}

	private boolean isInteger(String text) {
		try {
			Integer.parseInt(text);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private void resetTextFiledsBorderColors() {
		filesDirectory.setBorder(new LineBorder(Color.GRAY, 1));
		outputWordsNumber.setBorder(new LineBorder(Color.GRAY, 1));
	}

}
