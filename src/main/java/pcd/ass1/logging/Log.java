package pcd.ass1.logging;

import java.time.Instant;

public class Log {
	
	private Log() {};
	
	public static void info(String text) {
		System.out.println(String.format("%s [INFO] %s %n", Instant.now(), text));
	}
	
	public static void info(String text, String caller) {
		System.out.println(String.format("%s [INFO] [%s] %s %n", Instant.now(), caller, text));
	}
	
	public static void error(String text) {
		System.err.println(String.format("%s [INFO] %s %n", Instant.now(), text));
	}
	
	public static void error(String text, String caller) {
		System.out.println(String.format("%s [INFO] [%s] %s %n", Instant.now(), caller, text));
	}

}
