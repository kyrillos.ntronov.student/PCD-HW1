package pcd.ass1.wordcounter.driver;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import pcd.ass1.gui.WordCounterParameters;
import pcd.ass1.logging.Log;
import pcd.ass1.wordcounter.agent.WordCounterAgent;
import pcd.ass1.wordcounter.agent.task.CountWordsTask;
import pcd.ass1.wordcounter.agent.task.ReadPDFTask;
import pcd.ass1.wordcounter.agent.task.WordCounterTask;
import pcd.ass1.wordcounter.agent.task.result.CountWordsResult;
import pcd.ass1.wordcounter.agent.task.result.JobResult;
import pcd.ass1.wordcounter.agent.task.result.ReadPDFResult;
import pcd.ass1.wordcounter.structures.data.Page;
import pcd.ass1.wordcounter.structures.data.WordCounterEntry;
import pcd.ass1.wordcounter.structures.monitors.ThreadSharedVariables;
import pcd.ass1.wordcounter.structures.monitors.WordCounterJobQueue;
import pcd.ass1.wordcounter.structures.monitors.WordCounterMap;
import pcd.ass1.wordcounter.structures.monitors.WordCounterResultsQueue;

public class WordCounterDriver extends SwingWorker<List<WordCounterEntry>, List<WordCounterEntry>> {

	private final int workersNumber;
	private final int wordsNumber;
	private final File baseDirectory;
	private final File exclusionFile;

	private final List<String> excludedWords;
	private final List<WordCounterAgent> workers;
	private final WordCounterMap wordCountMap;
	private final WordCounterJobQueue jobQueue;
	private final WordCounterResultsQueue resultsQueue;

	private final ThreadSharedVariables threadSharedVariables;

	private final JTextArea outputTextArea;
	private final JTextField wordCountField;
	private final JButton startButton;

	private Instant jobStart;

	public WordCounterDriver(final WordCounterParameters parameters, JTextArea outputTextArea,
			JTextField wordCountField, JButton startButton) {
		this.wordsNumber = parameters.getMostFrequentWordsNumber();
		this.workersNumber = parameters.getVirtualCoresNumber();
		this.baseDirectory = parameters.getDirectory();
		this.exclusionFile = parameters.getExclusionFile();
		this.excludedWords = new LinkedList<>();
		this.workers = new LinkedList<>();

		this.threadSharedVariables = new ThreadSharedVariables();
		this.wordCountMap = new WordCounterMap(threadSharedVariables, wordsNumber);
		this.jobQueue = new WordCounterJobQueue(threadSharedVariables);
		this.resultsQueue = new WordCounterResultsQueue(threadSharedVariables);

		this.outputTextArea = outputTextArea;
		this.wordCountField = wordCountField;
		this.startButton = startButton;
	}

	@Override
	public List<WordCounterEntry> doInBackground() throws Exception {
		if (!workers.isEmpty()) {
			Log.error("Cannot start another count while previous is still in progress!", "MAIN");
			return new ArrayList<>();
		} else {
			outputTextArea.setText("Reading files, please wait...");
			countWordOccurencies();
			return wordCountMap.getTopWords();
		}
	}

	@Override
	public void process(List<List<WordCounterEntry>> chunks) {
		Log.info("PUBLISHING CHUNKS", "MAIN");
		outputTextArea.setText("");
		List<WordCounterEntry> chunk = chunks.get(chunks.size() - 1);
		for (WordCounterEntry entry : chunk) {
			outputTextArea.append(String.format("%s - %s %n", entry.getWord(), entry.getWordOccurency()));
		}
		wordCountField.setText(String.valueOf("Words Counted: " + wordCountMap.getWordsCounted()));
	}

	@Override
	public void done() {
		outputTextArea.setText("-----Done Counting-----\n" + outputTextArea.getText());
		startButton.setEnabled(true);
	}

	public void pauseCount() {
		if (!workers.isEmpty() && !threadSharedVariables.isThreadsPaused()) {
			threadSharedVariables.setThreadsPaused(true);
			Log.info("Computations paused", "MAIN");
		} else {
			Log.info("Cannot pause workers, no word count is in progress", "MAIN");
		}
	}

	public void resumeCount() {
		if (!workers.isEmpty() && threadSharedVariables.isThreadsPaused()) {
			threadSharedVariables.setThreadsPaused(false);
			wordCountMap.notifyAllPausedThreads();
			jobQueue.notifyAllPausedThreads();
			resultsQueue.notifyAllPausedThreads();
			Log.info("Computations resumed", "MAIN");
		} else {
			Log.info("Cannot resume workers, word count is in progress", "MAIN");
		}
	}

	private void countWordOccurencies() throws InterruptedException {
		resetCount();
		loadExclusionFile();
		loadReadJobs();
		createAndStartWorkers();
		processResults();
		terminateCount();
	}

	private void resetCount() throws InterruptedException {
		wordCountMap.clear();
		jobQueue.clear();
		excludedWords.clear();
		workers.clear();
		jobStart = Instant.now();
		Log.info(String.format("Job started with %s workers", workersNumber), "MAIN");
	}

	private void loadExclusionFile() {
		if (exclusionFile != null) {
			try {
				String content = new String(Files.readAllBytes(Paths.get(exclusionFile.getPath())));
				excludedWords.addAll(Arrays.asList(content.split("\n")));
				Log.info(String.format("Excluded words file found: %s", excludedWords), "MAIN");
			} catch (IOException e) {
				throw new IllegalStateException("IO Exception encountered: " + e);
			}
		} else {
			Log.info("Excluded words file was not provided", "MAIN");
		}
	}

	private void loadReadJobs() throws InterruptedException {
		List<String> paths = getPdfFilesPaths();

		for (int i = 0; i < paths.size(); i++) {
			String filePath = paths.get(i);
			WordCounterTask job = new ReadPDFTask(filePath);
			jobQueue.add(job);
		}
	}

	private List<String> getPdfFilesPaths() {
		List<String> paths = new LinkedList<>();
		for (String file : findFiles()) {
			paths.add(baseDirectory + "/" + file);
		}
		return paths;
	}

	private void createAndStartWorkers() {
		for (int i = 0; i < workersNumber; i++) {
			WordCounterAgent worker = new WordCounterAgent(String.format("WORKER-%s", i), jobQueue, resultsQueue,
					threadSharedVariables);
			workers.add(worker);
			worker.start();
		}
		Log.info(String.format("Threads running: %s", Thread.activeCount()), "MAIN");
	}

	private List<String> findFiles() {
		List<String> files = new LinkedList<>();
		for (String file : baseDirectory.list()) {
			if (file.endsWith(".pdf")) {
				files.add(file);
			}
		}
		return files;
	}

	private void processResults() throws InterruptedException {
		while (jobQueue.hasNext() || resultsQueue.hasNext() || isAnyAgentWorking()) {
			attemptToGetAndHandleNextJobResult();
		}
		threadSharedVariables.setDriverFinished(true);
		Log.info("Driver finished handling all results", "DRIVER");
	}

	private boolean isAnyAgentWorking() {
		return workers.stream().anyMatch(WordCounterAgent::isWorking);
	}

	private void attemptToGetAndHandleNextJobResult() throws InterruptedException {
		JobResult nextResult = resultsQueue.poll();
		if (nextResult != null) {
			handleJobResult(nextResult);
		}
		sleep(10);
	}

	private void handleJobResult(JobResult jobResult) throws InterruptedException {
		if (jobResult instanceof ReadPDFResult) {
			ReadPDFResult readResult = (ReadPDFResult) jobResult;
			for (Page page : readResult.getParsedPages()) {
				jobQueue.add(new CountWordsTask(page, excludedWords));
			}
		} else if (jobResult instanceof CountWordsResult) {
			CountWordsResult countResult = (CountWordsResult) jobResult;
			wordCountMap.registerWordOccurences(countResult.getCountMap());
			publish(wordCountMap.getTopWords());
		} else {
			throw new IllegalArgumentException("Job result is not recognized");
		}
	}

	private void terminateCount() throws InterruptedException {
		for (WordCounterAgent worker : workers) {
			worker.join();
		}
		workers.clear();
		Instant countFinish = Instant.now();
		Log.info(String.format("Finished Counting Words in %s ms", Duration.between(jobStart, countFinish).toMillis()),
				"MAIN");
		Log.info("Output: counted " + wordCountMap.getWordsCounted() + " top words " + wordCountMap.getTopWords(),
				"MAIN");
	}

	private void sleep(int ms) throws InterruptedException {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			throw e;
		}
	}

}
