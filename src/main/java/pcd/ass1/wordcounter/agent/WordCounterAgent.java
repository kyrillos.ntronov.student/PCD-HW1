package pcd.ass1.wordcounter.agent;

import pcd.ass1.logging.Log;
import pcd.ass1.wordcounter.agent.task.WordCounterTask;
import pcd.ass1.wordcounter.agent.task.result.JobResult;
import pcd.ass1.wordcounter.structures.monitors.ThreadSharedVariables;
import pcd.ass1.wordcounter.structures.monitors.WordCounterJobQueue;
import pcd.ass1.wordcounter.structures.monitors.WordCounterResultsQueue;

public class WordCounterAgent extends Thread {

	private final String name;

	private final WordCounterJobQueue jobQueue;
	private final WordCounterResultsQueue resultsQueue;
	private final ThreadSharedVariables sharedVariables;

	private boolean isWorking;

	public WordCounterAgent(String name, WordCounterJobQueue jobQueue, WordCounterResultsQueue resultsQueue,
			ThreadSharedVariables sharedVariables) {
		this.name = name;
		this.jobQueue = jobQueue;
		this.resultsQueue = resultsQueue;
		this.sharedVariables = sharedVariables;

		this.isWorking = false;
	}

	public boolean isWorking() {
		return isWorking;
	}

	public void setWorking(boolean isWorking) {
		this.isWorking = isWorking;
	}

	@Override
	public void run() {
		try {
			while (isRunning()) {
				attemptToGetAndRunNextJob();
			}
			Log.info("Worker finished all jobs, shutting down", name);
		} catch (InterruptedException e) {
			Log.error("Interrupted Exception Encountered, Fatal Error", "FATAL");
			this.interrupt();
		}
	}

	private boolean isRunning() throws InterruptedException {
		return jobQueue.hasNext() || resultsQueue.hasNext() || !sharedVariables.isDriverFinished();
	}

	private void attemptToGetAndRunNextJob() throws InterruptedException {
		WordCounterTask nextJob = jobQueue.pollAndSetWorkingIfPresent(this);
		if (nextJob != null) {
			nextJob.setWorkerName(name);
			Log.info(String.format("Beginning job %s", nextJob.getJobName()), name);
			JobResult result = nextJob.executeJob();
			resultsQueue.add(result);
			Log.info(String.format("Finished job %s", nextJob.getJobName()), name);
			this.isWorking = false;
		} else {
			sleep();
		}
	}

	private void sleep() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			this.interrupt();
		}
	}

}
