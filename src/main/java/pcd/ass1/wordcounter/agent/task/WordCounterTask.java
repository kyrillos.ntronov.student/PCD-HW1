package pcd.ass1.wordcounter.agent.task;

import pcd.ass1.wordcounter.agent.task.result.JobResult;

public interface WordCounterTask {
	
	public JobResult executeJob() throws InterruptedException;

	public String getJobName();
	
	public void setWorkerName(String workerName);

}
