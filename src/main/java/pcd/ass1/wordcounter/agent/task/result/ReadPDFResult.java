package pcd.ass1.wordcounter.agent.task.result;

import java.util.List;

import pcd.ass1.wordcounter.structures.data.Page;

public class ReadPDFResult implements JobResult {

	private final List<Page> parsedPages;

	public ReadPDFResult(List<Page> parsedPages) {
		super();
		this.parsedPages = parsedPages;
	}

	public List<Page> getParsedPages() {
		return parsedPages;
	}

}
