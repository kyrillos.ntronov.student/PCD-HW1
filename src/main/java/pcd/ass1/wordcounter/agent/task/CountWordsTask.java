package pcd.ass1.wordcounter.agent.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pcd.ass1.logging.Log;
import pcd.ass1.wordcounter.agent.task.result.CountWordsResult;
import pcd.ass1.wordcounter.agent.task.result.JobResult;
import pcd.ass1.wordcounter.structures.data.Page;

/**
 * Parses pages and updates provided counter map
 *
 */
public class CountWordsTask implements WordCounterTask {

	private static final String NAME = "PARSE_TEXT";
	private String workerName;

	private final Page page;

	private final List<String> excludedWords;

	private final Map<String, Integer> countMap;

	public CountWordsTask(final Page page, final List<String> excludedWords) {
		this.workerName = "undefined";
		this.page = page;
		this.excludedWords = excludedWords;
		this.countMap = new HashMap<>();
	}

	@Override
	public JobResult executeJob() throws InterruptedException {
		return countWordsInPage();
	}

	@Override
	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}

	@Override
	public String getJobName() {
		return NAME;
	}

	private CountWordsResult countWordsInPage() {
		Log.info("Starting parsing page", String.format("%s-%s", NAME, workerName));
		for (String rawWord : page.getWords()) {
			String word = rawWord.toLowerCase();
			if (!word.isEmpty() && !excludedWords.contains(word)) {
				registerWordOccurence(word);
			}
		}
		Log.info("Finished parsing page", String.format("%s-%s", NAME, workerName));
		return new CountWordsResult(countMap);
	}

	private void registerWordOccurence(String word) {
		Integer wordOccurencies = countMap.get(word);
		if (wordOccurencies != null) {
			countMap.put(word, wordOccurencies + 1);
		} else {
			countMap.put(word, 1);
		}
	}

	@Override
	public String toString() {
		return "ParseTextWorker [name=" + NAME + "]";
	}

}
