package pcd.ass1.wordcounter.agent.task.result;

import java.util.Map;

public class CountWordsResult implements JobResult {

	private final Map<String, Integer> countMap;

	public CountWordsResult(Map<String, Integer> countMap) {
		super();
		this.countMap = countMap;
	}

	public Map<String, Integer> getCountMap() {
		return countMap;
	}

}
