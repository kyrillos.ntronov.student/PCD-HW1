package pcd.ass1.wordcounter.agent.task;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pcd.ass1.logging.Log;
import pcd.ass1.parser.PdfParser;
import pcd.ass1.parser.PdfParserException;
import pcd.ass1.wordcounter.agent.task.result.JobResult;
import pcd.ass1.wordcounter.agent.task.result.ReadPDFResult;
import pcd.ass1.wordcounter.structures.data.Page;

/**
 * Reads file and splits read text in pages.
 */
public class ReadPDFTask implements WordCounterTask {

	private static final int PAGE_SIZE = 5000;

	private static final String NAME = "READ_PDF";
	private String workerName;
	
	private final String filePath;

	public ReadPDFTask(final String filePath) {
		this.workerName = "undefined";
		this.filePath = filePath;
	}

	@Override
	public JobResult executeJob() throws InterruptedException {
		try {
			return makePages();
		} catch (PdfParserException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}

	@Override
	public String getJobName() {
		return NAME;
	}

	private ReadPDFResult makePages() throws PdfParserException {
		Log.info("Starting reading: " + filePath, String.format("%s-%s", NAME, workerName));
		List<Page> parsedPages = readPages(filePath);
		Log.info("Reading completed, added " + parsedPages.size() + " pages: " + filePath,
				String.format("%s-%s", NAME, workerName));
		return new ReadPDFResult(parsedPages);
	}

	private List<Page> readPages(final String filePath) throws PdfParserException {
		PdfParser parser = new PdfParser(filePath);
		String parsedText = parser.parse();
		
		Pattern p = Pattern.compile("[\\w']+");
		Matcher m = p.matcher(parsedText);

		List<String> words = new LinkedList<>();
		
		while ( m.find() ) {
		    String word = parsedText.substring(m.start(), m.end());
		    words.add(word.toLowerCase());
		}

		List<Page> pages = new LinkedList<>();
		for (int i = 0; i < words.size(); i += PAGE_SIZE) {
			Page page = new Page(words.subList(i, Math.min(i + PAGE_SIZE, words.size())));
			pages.add(page);
		}

		return pages;
	}

}
