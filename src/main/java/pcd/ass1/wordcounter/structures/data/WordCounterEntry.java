package pcd.ass1.wordcounter.structures.data;

public class WordCounterEntry {

	private final String word;

	private Integer wordOccurency;

	public WordCounterEntry(String word, Integer wordOccurency) {
		this.word = word;
		this.wordOccurency = wordOccurency;
	}

	public Integer getWordOccurency() {
		return wordOccurency;
	}

	public void incrementWordOccurency() {
		wordOccurency++;
	}

	public String getWord() {
		return word;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordCounterEntry other = (WordCounterEntry) obj;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[word=" + word + ", wordOccurency=" + wordOccurency + "]";
	}

}
