package pcd.ass1.wordcounter.structures.monitors;

import java.util.LinkedList;
import java.util.Queue;

import pcd.ass1.logging.Log;
import pcd.ass1.wordcounter.agent.WordCounterAgent;

public abstract class MonitorQueue<T> {

	private final ThreadSharedVariables threadSharedVariables;

	private final Queue<T> queue;

	private String name;

	protected MonitorQueue(ThreadSharedVariables threadSharedVariables) {
		this.threadSharedVariables = threadSharedVariables;
		this.queue = new LinkedList<>();
		this.name = "undefined";
	}

	public void add(T item) throws InterruptedException {
		synchronized (queue) {
			waitIfPaused();
			queue.add(item);
		}
	}
	
	public T poll() throws InterruptedException {
		synchronized (queue) {
			waitIfPaused();
			return queue.poll();
		}
	}

	public T pollAndSetWorkingIfPresent(WordCounterAgent agent) throws InterruptedException {
		synchronized (queue) {
			waitIfPaused();
			T e = queue.poll();
			if(e != null) {
				agent.setWorking(true);
			}
			return e;
		}
	}

	public boolean hasNext() throws InterruptedException {
		synchronized (queue) {
			waitIfPaused();
			Log.info(String.format("%s has %s jobs left", name, queue.size()), "QUEUE_" + name);
			return queue.peek() != null;
		}
	}

	public void clear() throws InterruptedException {
		synchronized (queue) {
			waitIfPaused();
			queue.clear();
		}
	}

	protected String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	public void notifyAllPausedThreads() {
		synchronized (queue) {
			queue.notifyAll();
		}
	}
	
	private void waitIfPaused() throws InterruptedException {
		synchronized (queue) {
			while (threadSharedVariables.isThreadsPaused()) {
				queue.wait();
			}
		}
	}

}
