package pcd.ass1.wordcounter.structures.monitors;

import pcd.ass1.wordcounter.agent.task.WordCounterTask;

public class WordCounterJobQueue extends MonitorQueue<WordCounterTask> {

	public WordCounterJobQueue(ThreadSharedVariables threadSharedVariables) {
		super(threadSharedVariables);
		setName("JOB_QUEUE");
	}

}
