package pcd.ass1.wordcounter.structures.monitors;

import pcd.ass1.wordcounter.agent.task.result.JobResult;

public class WordCounterResultsQueue extends MonitorQueue<JobResult> {

	public WordCounterResultsQueue(ThreadSharedVariables threadSharedVariables) {
		super(threadSharedVariables);
		setName("RESULT_QUEUE");
	}

}
