package pcd.ass1.wordcounter.structures.monitors;

public class ThreadSharedVariables {

	private boolean isThreadsPaused;
	private boolean isDriverFinished;

	public ThreadSharedVariables() {
		isThreadsPaused = false;
		isDriverFinished = false;
	}

	public synchronized boolean isThreadsPaused() {
		return isThreadsPaused;
	}

	public synchronized void setThreadsPaused(boolean isThreadsPaused) {
		this.isThreadsPaused = isThreadsPaused;
	}

	public synchronized boolean isDriverFinished() {
		return isDriverFinished;
	}

	public synchronized void setDriverFinished(boolean isDriverFinished) {
		this.isDriverFinished = isDriverFinished;
	}
	
	@Override
	public String toString() {
		return "ThreadPausedFlag [isThreadsPaused=" + isThreadsPaused + "]";
	}

}
