package pcd.ass1.parser;

import java.util.List;

public class PdfParsedFile {

	private String fileName;

	private String fileText;

	private List<String> mostFrequentWords;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileText() {
		return fileText;
	}

	public void setFileText(String fileText) {
		this.fileText = fileText;
	}

	public List<String> getMostFrequentWords() {
		return mostFrequentWords;
	}

	public void setMostFrequentWords(List<String> mostFrequentWords) {
		this.mostFrequentWords = mostFrequentWords;
	}

}
