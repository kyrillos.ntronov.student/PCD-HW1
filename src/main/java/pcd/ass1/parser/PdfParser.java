package pcd.ass1.parser;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

public class PdfParser {

	private final String filePath;

	public PdfParser(final String filePath) {
		this.filePath = filePath;
	}

	public String parse() throws PdfParserException {
		File file = new File(filePath);
		if(!file.exists() || !file.canRead()) {
			throw new PdfParserException(String.format("File %s doesn't exist or is not readable.", filePath));
		}
		try (PDDocument document = PDDocument.load(file)) {
			PDFTextStripper pdfStripper = new PDFTextStripper();
			return pdfStripper.getText(document);
		} catch (IOException e) {
			throw new PdfParserException(e);
		}
	}

}
