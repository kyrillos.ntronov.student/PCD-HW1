package pcd.ass1.parser;

public class PdfParserException extends Exception {

	private static final long serialVersionUID = 3530478073983719742L;

	public PdfParserException() {
		super();
	}

	public PdfParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PdfParserException(String message, Throwable cause) {
		super(message, cause);
	}

	public PdfParserException(String message) {
		super(message);
	}

	public PdfParserException(Throwable cause) {
		super(cause);
	}

}
