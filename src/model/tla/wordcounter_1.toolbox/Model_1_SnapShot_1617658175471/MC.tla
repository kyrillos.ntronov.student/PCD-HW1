---- MODULE MC ----
EXTENDS wordcounter, TLC

\* CONSTANT definitions @modelParameterConstants:0WorkersNumber
const_16176581711658000 == 
2
----

\* CONSTANT definitions @modelParameterConstants:1PagesPerFile
const_16176581711659000 == 
2
----

\* CONSTANT definitions @modelParameterConstants:2FilesNumber
const_161765817116510000 == 
2
----

=============================================================================
\* Modification History
\* Created Mon Apr 05 23:29:31 CEST 2021 by Cyrill
