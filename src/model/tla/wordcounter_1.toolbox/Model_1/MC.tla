---- MODULE MC ----
EXTENDS wordcounter, TLC

\* CONSTANT definitions @modelParameterConstants:0WorkersNumber
const_161765869232020000 == 
2
----

\* CONSTANT definitions @modelParameterConstants:1PagesPerFile
const_161765869232021000 == 
2
----

\* CONSTANT definitions @modelParameterConstants:2FilesNumber
const_161765869232022000 == 
2
----

=============================================================================
\* Modification History
\* Created Mon Apr 05 23:38:12 CEST 2021 by Cyrill
