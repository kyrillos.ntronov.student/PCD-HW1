---------------------------- MODULE wordcounter ----------------------------

EXTENDS TLC, Integers, Sequences
CONSTANTS FilesNumber, PagesPerFile, WorkersNumber

ASSUME FilesNumber > 0
ASSUME PagesPerFile > 0
ASSUME WorkersNumber > 0

(*--algorithm wordcounter


variables
    isSetUp = FALSE;

    jobQueue = <<>>;
    resultQueue = <<>>;
    isRunning = TRUE;

    agentsWorkingLocks = <<>>;

    pagesRead = 0;

define
    StopsRunningProperty == <>[](isRunning = FALSE)
    PagesReadProperty == <>[](pagesRead = FilesNumber * PagesPerFile)
    AllWorkingLocksReleased == <>[](agentsWorkingLocks = <<>>)
end define;

fair process setUp = 0
variables
    i = 1;
begin SetUp:
    while i <= FilesNumber do
        jobQueue := Append(jobQueue, "read-job");
        i := i + 1;
    end while;
    assert Len(jobQueue) = FilesNumber;
    isSetUp := TRUE;
end process;

fair process driver = 1
variables
    result = "";
begin Driver:
await isSetUp;
runDriver:
    while isRunning do
        takeResult:
            if resultQueue /= <<>> then
                m1:
                    result := Head(resultQueue);
                    resultQueue := Tail(resultQueue);
                
            checkTermination:
                if resultQueue = <<>> /\ jobQueue = <<>> /\ agentsWorkingLocks = <<>> then
                    m2: 
                        isRunning := FALSE;
                end if;
                
            consumeResult:
                if result = "read-result" then
                    m3:
                        jobQueue := Append(jobQueue, "count-job");
                else
                    m4:
                        pagesRead := pagesRead + 1;
                end if;
             end if;
    end while;
    
end process;


fair process agent \in 1..WorkersNumber
variables
    job = "";
    j = -1;
begin Agent:
await isSetUp;
runAgent:
    while isRunning do
        takeJob:
            if jobQueue /= <<>> then
            l1:
                agentsWorkingLocks := Append(agentsWorkingLocks, "lock");
                job := Head(jobQueue);
                jobQueue := Tail(jobQueue);
                
                consumeJob:
                    if job = "read-job" then
                        l2:
                            j := 1;
                            l3:
                                while j <= PagesPerFile do
                                    resultQueue := Append(resultQueue, "read-result");
                                    j := j + 1;
                                end while;
                            agentsWorkingLocks := Tail(agentsWorkingLocks);
                    else
                        l4:
                            resultQueue := Append(resultQueue, "count-result");
                            agentsWorkingLocks := Tail(agentsWorkingLocks);
                    end if;
            end if;
    end while;
end process;

end algorithm;*)
\* BEGIN TRANSLATION (chksum(pcal) = "2f24a604" /\ chksum(tla) = "c385dc86")
VARIABLES isSetUp, jobQueue, resultQueue, isRunning, agentsWorkingLocks, 
          pagesRead, pc

(* define statement *)
StopsRunningProperty == <>[](isRunning = FALSE)
PagesReadProperty == <>[](pagesRead = FilesNumber * PagesPerFile)
AllWorkingLocksReleased == <>[](agentsWorkingLocks = <<>>)

VARIABLES i, result, job, j

vars == << isSetUp, jobQueue, resultQueue, isRunning, agentsWorkingLocks, 
           pagesRead, pc, i, result, job, j >>

ProcSet == {0} \cup {1} \cup (1..WorkersNumber)

Init == (* Global variables *)
        /\ isSetUp = FALSE
        /\ jobQueue = <<>>
        /\ resultQueue = <<>>
        /\ isRunning = TRUE
        /\ agentsWorkingLocks = <<>>
        /\ pagesRead = 0
        (* Process setUp *)
        /\ i = 1
        (* Process driver *)
        /\ result = ""
        (* Process agent *)
        /\ job = [self \in 1..WorkersNumber |-> ""]
        /\ j = [self \in 1..WorkersNumber |-> -1]
        /\ pc = [self \in ProcSet |-> CASE self = 0 -> "SetUp"
                                        [] self = 1 -> "Driver"
                                        [] self \in 1..WorkersNumber -> "Agent"]

SetUp == /\ pc[0] = "SetUp"
         /\ IF i <= FilesNumber
               THEN /\ jobQueue' = Append(jobQueue, "read-job")
                    /\ i' = i + 1
                    /\ pc' = [pc EXCEPT ![0] = "SetUp"]
                    /\ UNCHANGED isSetUp
               ELSE /\ Assert(Len(jobQueue) = FilesNumber, 
                              "Failure of assertion at line 38, column 5.")
                    /\ isSetUp' = TRUE
                    /\ pc' = [pc EXCEPT ![0] = "Done"]
                    /\ UNCHANGED << jobQueue, i >>
         /\ UNCHANGED << resultQueue, isRunning, agentsWorkingLocks, pagesRead, 
                         result, job, j >>

setUp == SetUp

Driver == /\ pc[1] = "Driver"
          /\ isSetUp
          /\ pc' = [pc EXCEPT ![1] = "runDriver"]
          /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                          agentsWorkingLocks, pagesRead, i, result, job, j >>

runDriver == /\ pc[1] = "runDriver"
             /\ IF isRunning
                   THEN /\ pc' = [pc EXCEPT ![1] = "takeResult"]
                   ELSE /\ pc' = [pc EXCEPT ![1] = "Done"]
             /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                             agentsWorkingLocks, pagesRead, i, result, job, j >>

takeResult == /\ pc[1] = "takeResult"
              /\ IF resultQueue /= <<>>
                    THEN /\ pc' = [pc EXCEPT ![1] = "m1"]
                    ELSE /\ pc' = [pc EXCEPT ![1] = "runDriver"]
              /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                              agentsWorkingLocks, pagesRead, i, result, job, j >>

m1 == /\ pc[1] = "m1"
      /\ result' = Head(resultQueue)
      /\ resultQueue' = Tail(resultQueue)
      /\ pc' = [pc EXCEPT ![1] = "checkTermination"]
      /\ UNCHANGED << isSetUp, jobQueue, isRunning, agentsWorkingLocks, 
                      pagesRead, i, job, j >>

checkTermination == /\ pc[1] = "checkTermination"
                    /\ IF resultQueue = <<>> /\ jobQueue = <<>> /\ agentsWorkingLocks = <<>>
                          THEN /\ pc' = [pc EXCEPT ![1] = "m2"]
                          ELSE /\ pc' = [pc EXCEPT ![1] = "consumeResult"]
                    /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                                    agentsWorkingLocks, pagesRead, i, result, 
                                    job, j >>

m2 == /\ pc[1] = "m2"
      /\ isRunning' = FALSE
      /\ pc' = [pc EXCEPT ![1] = "consumeResult"]
      /\ UNCHANGED << isSetUp, jobQueue, resultQueue, agentsWorkingLocks, 
                      pagesRead, i, result, job, j >>

consumeResult == /\ pc[1] = "consumeResult"
                 /\ IF result = "read-result"
                       THEN /\ pc' = [pc EXCEPT ![1] = "m3"]
                       ELSE /\ pc' = [pc EXCEPT ![1] = "m4"]
                 /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                                 agentsWorkingLocks, pagesRead, i, result, job, 
                                 j >>

m3 == /\ pc[1] = "m3"
      /\ jobQueue' = Append(jobQueue, "count-job")
      /\ pc' = [pc EXCEPT ![1] = "runDriver"]
      /\ UNCHANGED << isSetUp, resultQueue, isRunning, agentsWorkingLocks, 
                      pagesRead, i, result, job, j >>

m4 == /\ pc[1] = "m4"
      /\ pagesRead' = pagesRead + 1
      /\ pc' = [pc EXCEPT ![1] = "runDriver"]
      /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                      agentsWorkingLocks, i, result, job, j >>

driver == Driver \/ runDriver \/ takeResult \/ m1 \/ checkTermination \/ m2
             \/ consumeResult \/ m3 \/ m4

Agent(self) == /\ pc[self] = "Agent"
               /\ isSetUp
               /\ pc' = [pc EXCEPT ![self] = "runAgent"]
               /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                               agentsWorkingLocks, pagesRead, i, result, job, 
                               j >>

runAgent(self) == /\ pc[self] = "runAgent"
                  /\ IF isRunning
                        THEN /\ pc' = [pc EXCEPT ![self] = "takeJob"]
                        ELSE /\ pc' = [pc EXCEPT ![self] = "Done"]
                  /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                                  agentsWorkingLocks, pagesRead, i, result, 
                                  job, j >>

takeJob(self) == /\ pc[self] = "takeJob"
                 /\ IF jobQueue /= <<>>
                       THEN /\ pc' = [pc EXCEPT ![self] = "l1"]
                       ELSE /\ pc' = [pc EXCEPT ![self] = "runAgent"]
                 /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                                 agentsWorkingLocks, pagesRead, i, result, job, 
                                 j >>

l1(self) == /\ pc[self] = "l1"
            /\ agentsWorkingLocks' = Append(agentsWorkingLocks, "lock")
            /\ job' = [job EXCEPT ![self] = Head(jobQueue)]
            /\ jobQueue' = Tail(jobQueue)
            /\ pc' = [pc EXCEPT ![self] = "consumeJob"]
            /\ UNCHANGED << isSetUp, resultQueue, isRunning, pagesRead, i, 
                            result, j >>

consumeJob(self) == /\ pc[self] = "consumeJob"
                    /\ IF job[self] = "read-job"
                          THEN /\ pc' = [pc EXCEPT ![self] = "l2"]
                          ELSE /\ pc' = [pc EXCEPT ![self] = "l4"]
                    /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                                    agentsWorkingLocks, pagesRead, i, result, 
                                    job, j >>

l2(self) == /\ pc[self] = "l2"
            /\ j' = [j EXCEPT ![self] = 1]
            /\ pc' = [pc EXCEPT ![self] = "l3"]
            /\ UNCHANGED << isSetUp, jobQueue, resultQueue, isRunning, 
                            agentsWorkingLocks, pagesRead, i, result, job >>

l3(self) == /\ pc[self] = "l3"
            /\ IF j[self] <= PagesPerFile
                  THEN /\ resultQueue' = Append(resultQueue, "read-result")
                       /\ j' = [j EXCEPT ![self] = j[self] + 1]
                       /\ pc' = [pc EXCEPT ![self] = "l3"]
                       /\ UNCHANGED agentsWorkingLocks
                  ELSE /\ agentsWorkingLocks' = Tail(agentsWorkingLocks)
                       /\ pc' = [pc EXCEPT ![self] = "runAgent"]
                       /\ UNCHANGED << resultQueue, j >>
            /\ UNCHANGED << isSetUp, jobQueue, isRunning, pagesRead, i, result, 
                            job >>

l4(self) == /\ pc[self] = "l4"
            /\ resultQueue' = Append(resultQueue, "count-result")
            /\ agentsWorkingLocks' = Tail(agentsWorkingLocks)
            /\ pc' = [pc EXCEPT ![self] = "runAgent"]
            /\ UNCHANGED << isSetUp, jobQueue, isRunning, pagesRead, i, result, 
                            job, j >>

agent(self) == Agent(self) \/ runAgent(self) \/ takeJob(self) \/ l1(self)
                  \/ consumeJob(self) \/ l2(self) \/ l3(self) \/ l4(self)

(* Allow infinite stuttering to prevent deadlock on termination. *)
Terminating == /\ \A self \in ProcSet: pc[self] = "Done"
               /\ UNCHANGED vars

Next == setUp \/ driver
           \/ (\E self \in 1..WorkersNumber: agent(self))
           \/ Terminating

Spec == /\ Init /\ [][Next]_vars
        /\ WF_vars(setUp)
        /\ WF_vars(driver)
        /\ \A self \in 1..WorkersNumber : WF_vars(agent(self))

Termination == <>(\A self \in ProcSet: pc[self] = "Done")

\* END TRANSLATION 

=============================================================================
\* Modification History
\* Last modified Mon Apr 05 23:33:50 CEST 2021 by Cyrill
\* Created Mon Apr 05 21:23:38 CEST 2021 by Cyrill
